# Cabin List

## Full Facility

| Name            | Size      | Location            |
|-----------------|-----------|---------------------|
| THOR’S LODGE    | Sleeps 16 | du Nord Village     |
| LYNX LODGE      | Sleeps 14 | du Nord Village     |
| GREENSTONE      | Sleeps 11 | Pine Pointe Village |
| NORTHERN LIGHTS | Sleeps 11 | Pine Pointe Village |
| LUNTA LOFT      | Sleeps 10 | du Nord Village     |
| DANE LODGE      | Sleeps 10 | du Nord Village     |
| THE WINDS       | Sleeps 10 | du Nord Village     |
| TWILIGHT        | Sleeps 7  | Northland Village   |
| SUNSHINE        | Sleeps 7  | Northland Village   |
| WHITE PINE      | Sleeps 7  | Northland Village   |
| NORTH STAR      | Sleeps 7  | Northland Village   |
| JACK’S II       | Sleeps 7  | du Nord Village     |
| BALD EAGLE      | Sleeps 7  | Pine Pointe Village |
| OSPREY          | Sleeps 7  | Pine Pointe Village |
| TIMBER WOLF     | Sleeps 7  | Pine Pointe Village |
| SKIPPER’S       | Sleeps 5  | Northland Village   |
| NORTH COVE      | Sleeps 5  | du Nord Village     |
| CHICKADEE       | Sleeps 5  | Pine Pointe Village |
| SPLIT ROCK      | Sleeps 5  | Pine Pointe Village |
| RAVEN           | Sleeps 5  | Pine Pointe Village |
| KAWINIPI        | Sleeps 4  | Northland Village   |
| WINTERGREEN     | Sleeps 4  | Pine Pointe Village |

## Rustic

| Name         | Size                     | Location          |
|--------------|--------------------------|-------------------|
| SANS SOUCI   | Sleeps 2 (+ 2 in summer) | du Nord Village   |
| SALT LICK    | Sleeps 6                 | du Nord Village   |
| MCKENZIE     | Sleeps 5                 | Northland Village |
| NUGGET       | Sleeps 4                 | du Nord Village   |
| SIAM         | Sleeps 4                 | du Nord Village   |
| TOTEM        | Sleeps 4                 | du Nord Village   |
| LUNA         | Sleeps 4                 | Northland Village |
| PILOT HOUSE  | Sleeps 2                 | du Nord Village   |
| EAGLE’S NEST | Sleeps 2                 | Northland Village |

## Platform Cabins

| Name                               | Size     | Location            |
|------------------------------------|----------|---------------------|
| LADYSLIPPER, MOOSE, GOSHAWK & BUCK | Sleeps 6 | Pine Pointe Village |
| DEER                               | Sleeps 6 | du Nord Village     |

