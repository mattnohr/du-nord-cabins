# Camp du Nord Cabins

There is an ongoing project to digitize the floorplans for the cabins at Camp du Nord. This repository is a place to store the content and enable collaboration.

## Want to Jump Right In?

[Click here for the list of cabins](https://gitlab.com/mattnohr/du-nord-cabins/-/issues)

## Helping Out

Thanks for helping fix the floor plans for YMCA Camp du Nord. The intent is to take these drawings and add them to the website so visitors can get a better understanding of each cabin.

These drawings were taken from old blueprints or from measurements made of the cabins. While the general measurements are probably right, I need help:

- Verifying the location of furniture
- Adding any missing labels
- Double-checking the rooms are correct
- Anything else you find!

## How to Help

Check out this video on how to help out:

[![Overview](http://img.youtube.com/vi/LtfXmQfVPHw/0.jpg)](http://www.youtube.com/watch?v=LtfXmQfVPHw "Overview")

### 1. Create a GitLab Account

To make suggestiongs, you will first need a free GitLab account. 

Sign up here: https://gitlab.com/users/sign_up

### 2. Finding Floor Plan Drawings

In GitLab, all the cabins and floor plans are in GitLab issues. Once you have an account, you can find a list of all the cabins here:

https://gitlab.com/mattnohr/du-nord-cabins/-/boards/1949095 

On any of the cabins, you can open the "issue" (cabin) and you will find a "design" for the floor plan. Click on the design for a larger view of the floor plan.

![](readme-images/designs-on-issues.png)

### 3. Making Suggestions

Once you are looking at the full floor plan, you can make as many suggestions as needed. Click anywhere you want to make a note. You can also respond to other notes/comments that people have left.

![](readme-images/design-view.png)

### Why GitLab?

First of all, Matt Nohr works for GitLab and wants to use the product for anything he can! 

That said, the commenting system on images/designs is something that GitLab provides for free that most tools require you to pay for. GitLab is built for this type of collaboration!

## Links

At a Glance:

- [List By Village](https://gitlab.com/mattnohr/du-nord-cabins/-/boards/1949095)
- [List By Status](https://gitlab.com/mattnohr/du-nord-cabins/-/boards/1949132)

By Village:

- [du Nord](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=village%3A%3Adu-nord)
- [Northland](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=village%3A%3Anorthland)
- [Pine Point](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=village%3A%3Apine-point)

By Status:

- [Initial Floorplans Created](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=progress%3A%3Ainital-floorplan-done)
- [Have Blueprints](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=progress%3A%3Ahave-blueprints) (but not digitized yet)
- [Missing Blueprints](https://gitlab.com/mattnohr/du-nord-cabins/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=progress%3A%3Amissing-blueprints)
